package target

import (
	"fmt"

	targetJsonDriver "gitlab.com/arthur_muraro/scope-manager/pkg/json"
)

// Public Scope

type Target struct {
	Host  string `json:"host"`
	Alias string `json:"alias"`
}

func AddTarget(path, host, alias string) {
	if ResolveAlias(path, alias) != "" {
		fmt.Printf("The following Alias can not be set, alias already exists: %s\n", alias)
		return
	}
	targetJsonDriver.WriteGenericDataToJson[Target](path, 'a', Target{Host: host, Alias: alias})
}

func RemoveTarget(path, input string) {
	if ResolveAlias(path, input) == "" && !HostExists(path, input) {
		fmt.Printf("The following value can not be removed, value does not exist: %s\n", input)
		return
	}

	Targets := *targetJsonDriver.ReadGenericArrayFromJson[Target](path)
	// Remove the target from the target list if it exists
	for i, item := range Targets {
		if item.Alias == input || item.Host == input {
			Targets[i] = Targets[len(Targets)-1]
			Targets = Targets[:len(Targets)-1]
		}
	}
	targetJsonDriver.WriteGenericDataToJson[Target](path, 'w', Targets...)
}

func ResolveAlias(path, alias string) string {
	Targets := targetJsonDriver.ReadGenericArrayFromJson[Target](path)
	for i := range *Targets {
		if (*Targets)[i].Alias == alias {
			return (*Targets)[i].Host
		}
	}
	return ""
}

func HostExists(path, Host string) bool {
	Targets := targetJsonDriver.ReadGenericArrayFromJson[Target](path)
	for i := range *Targets {
		if (*Targets)[i].Host == Host {
			return true
		}
	}
	return false
}
