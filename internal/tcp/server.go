package tcp

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"strconv"
	"strings"
	"sync"
)

// TcpConfiguration represents the configuration for the TCP service.
type TcpConfiguration struct {
	ListenAddr      string                  // IP address to listen on
	ListenPort      uint16                  // Port to listen on
	Listener        *net.Listener           // TCP server instance
	Mux             sync.Mutex              // Mux to access ClientsSessions safely
	ClientsSessions map[uint]*ClientSession // List of ClientsSessions instances
}

// ClientSession represent one client session of the TCP service.
type ClientSession struct {
	RemoteAddr   string
	RemotePort   uint16
	RemoteUser   string        // Stores the user owner of the remote shell
	OS           string        // Stores the OS name
	Terminal     string        // Store the name of the terminal detected
	ClientOutput chan string   // The channel used to pass output of commands
	ServerInput  chan string   // The channel used to pass input commands
	Closed       chan struct{} // Used to detect when a session has ended
	Id           uint          // Session's Id
	ShellMode    bool          // Used to change the way ClientOutput is handled
	conn         net.Conn      // The TCP connection itself
}

// Server Constructor.
func NewTcpConfiguration(listenAddr string) *TcpConfiguration {
	parsedAddr := strings.Split(listenAddr, ":")
	port, _ := strconv.ParseUint(parsedAddr[1], 10, 16)
	return &TcpConfiguration{
		ListenAddr:      parsedAddr[0],
		ListenPort:      uint16(port),
		ClientsSessions: make(map[uint]*ClientSession),
	}

}

// Start initiates the TCP service.
func (serverInstance *TcpConfiguration) Start() (err error) {
	// Starts the TcpListener
	listenAddr := fmt.Sprintf("%s:%d", serverInstance.ListenAddr, serverInstance.ListenPort)
	listener, err := net.Listen("tcp", listenAddr)
	if err != nil {
		fmt.Println("Error starting server:", err)
		return
	}
	serverInstance.Listener = &listener
	defer (*serverInstance.Listener).Close()
	fmt.Printf("TCP server Listening on address \x1b[31m%s\x1b[0m:\x1b[31m%d\x1b[0m.\n", serverInstance.ListenAddr, serverInstance.ListenPort)

	// Waiting for incoming connections
	for {
		if serverInstance.Listener == nil {
			break
		}
		conn, err := (*serverInstance.Listener).Accept()
		if err != nil {
			continue
		}

		go serverInstance.handleClient(conn)
	}
	return
}

func (clientSession *ClientSession) SendCommand(cmd string) {
	clientSession.ServerInput <- cmd
}

func (serverInstance *TcpConfiguration) CloseClientSession(clientSessionId uint) {
	serverInstance.Mux.Lock()
	defer serverInstance.Mux.Unlock()
	close(serverInstance.ClientsSessions[clientSessionId].Closed)
}

func (serverInstance *TcpConfiguration) CloseAllClientSession() {
	serverInstance.Mux.Lock()
	defer serverInstance.Mux.Unlock()
	for _, session := range serverInstance.ClientsSessions {
		close(session.Closed)
	}
}

func (serverInstance *TcpConfiguration) Stop() {
	if serverInstance.Listener != nil {
		fmt.Printf("Closing TCP server on address \x1b[31m%s\x1b[0m:\x1b[31m%d\x1b[0m.\n", serverInstance.ListenAddr, serverInstance.ListenPort)
		serverInstance.CloseAllClientSession()
		(*serverInstance.Listener).Close()
		serverInstance.Listener = nil
		return
	}
	fmt.Println("The TCP service is not running")
}

// Private Scope

// ClientSession Constructor.
func newClientSession(conn *net.Conn, RemoteAddr string, id uint) *ClientSession {
	parsedAddr := strings.Split(RemoteAddr, ":")
	port, _ := strconv.ParseUint(parsedAddr[1], 10, 16)
	return &ClientSession{
		conn:         *conn,
		ServerInput:  make(chan string),
		ClientOutput: make(chan string),
		Closed:       make(chan struct{}),
		RemoteAddr:   parsedAddr[0],
		RemotePort:   uint16(port),
		ShellMode:    false,
		Id:           id,
	}
}

func (serverInstance *TcpConfiguration) closeClientSession(clientSessionId uint) {
	serverInstance.ClientsSessions[clientSessionId].conn.Close()
	serverInstance.Mux.Lock()
	delete(serverInstance.ClientsSessions, clientSessionId)
	serverInstance.Mux.Unlock()
	fmt.Printf("Session \x1b[31m%d\x1b[0m Closed.\n", clientSessionId)
}

func (serverInstance *TcpConfiguration) handleClient(conn net.Conn) {
	// Prevent Id duplication in map
	id := uint(1)
	for {
		if serverInstance.ClientsSessions[id] == nil {
			break
		}
		id++
	}
	serverInstance.Mux.Lock()
	serverInstance.ClientsSessions[id] = newClientSession(&conn, conn.RemoteAddr().String(), id)
	serverInstance.Mux.Unlock()
	client := serverInstance.ClientsSessions[id]
	fmt.Printf("New session received from \x1b[31m%s\x1b[0m:\x1b[31m%d\x1b[0m\n", client.RemoteAddr, client.RemotePort)

	defer serverInstance.closeClientSession(id)

	go client.receiveCommands()

	go client.instantiateSession()

	for {
		select {
		case <-client.Closed:
			return // Leave the goroutine once the clients is Closed
		case cmd := <-client.ServerInput:
			conn.Write([]byte(cmd + "\n"))
		}
	}
}

func (clientSession *ClientSession) receiveCommands() {
	const DefaultBufferSize = 8192
	for {
		buffer := bytes.NewBuffer(nil)
		chunk := make([]byte, DefaultBufferSize)
		read, err := clientSession.conn.Read(chunk)
		if err == io.EOF {
			close(clientSession.Closed)
			return
		}
		if err != nil {
			if strings.HasSuffix(err.Error(), "use of Closed network connection") {
				// If session has been killed
				return
			}
			return
		}

		buffer.Write(chunk[:read])

		text := buffer.String()
		out := strings.TrimSpace(text)
		if clientSession.ShellMode {
			fmt.Print(text)
		} else {
			clientSession.ClientOutput <- out
		}
	}
}
