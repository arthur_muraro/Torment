package tcp

import (
	"path/filepath"
	"strings"
)

// Private scope

func (clientSession *ClientSession) commandExists(c string) bool {
	cmd := "which " + c
	out := clientSession.SendJobCommand(cmd)
	return len(out) != 0
}

var spawnerList = []string{
	"python -c 'import pty; pty.spawn(\"__shell__\")'",
	"python3 -c 'import pty; pty.spawn(\"__shell__\")'",
	"python2.7 -c 'import pty; pty.spawn(\"__shell__\")'",
	"perl -e 'exec \"__shell__\";'",
	"ruby -e 'exec \"__shell__\"'",
	"lua -e 'os.execute(\"__shell__\")'",
	"/usr/bin/script -qc __shell__ /dev/null",
}

func (clientSession *ClientSession) spawnPty(shellPath string) bool {
	for i := range spawnerList {
		c, _, _ := strings.Cut(spawnerList[i], " ")
		if !clientSession.commandExists(c) {
			continue
		}
		cmd := strings.Replace(spawnerList[i], "__shell__", shellPath, 1)
		clientSession.SendCommand(cmd + " 2>&1")
		return true
	}
	return false
}

func (clientSession *ClientSession) detectBash() string {
	cmd := "cat /etc/shells"
	output := clientSession.SendJobCommand(cmd)
	for line := range output {
		if filepath.Base(output[line]) == "bash" {
			return output[line]
		}
	}
	return "/bin/sh"
}
