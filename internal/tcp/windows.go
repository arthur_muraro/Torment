package tcp

import (
	"fmt"
	"os"
	"time"

	"golang.org/x/crypto/ssh/terminal"
)

// Private scope

var powershells = []string{
	"pwsh",
	"pwsh.exe",
	"powershell",
	"powershell.exe",
}

func (clientSession *ClientSession) spawnPowershell() {
	for i := range powershells {
		output := clientSession.sendJobCommand("where "+powershells[i], "&")
		if len(output) > 0 {
			clientSession.SendCommand(powershells[i])
			time.Sleep(time.Second)
			clientSession.Terminal = "Powershell"
			return
		}
	}
	fmt.Println("Could Not spawn a powershell session")
}

func (clientSession *ClientSession) resizePowershellTty() {
	// Get local terminal size
	fd := int(os.Stdout.Fd())
	w, _, _ := terminal.GetSize(fd)

	// resize the remote terminal
	cmd1 := fmt.Sprintf("$Host.UI.RawUI.BufferSize = New-Object Management.Automation.Host.Size (%d, 24)", w)
	cmd2 := fmt.Sprintf("$host.UI.RawUI.WindowSize = New-Object -TypeName System.Management.Automation.Host.Size -ArgumentList (%d, 24)", w)

	clientSession.SendCommand(cmd1)
	clientSession.SendCommand(cmd2)
}
