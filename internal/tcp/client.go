package tcp

import (
	"fmt"
	"math/rand"
	"regexp"
	"strings"
	"time"
)

// Public Scope

func (clientSession *ClientSession) SendJobCommand(cmd string) []string {
	// If the remote session if a windows cmd.exe, the default separator is different than Unix based terminals and powershell
	if clientSession.Terminal == "Console" {
		return clientSession.sendJobCommand(cmd, "&")
	}
	return clientSession.sendJobCommand(cmd, ";")

}

func (clientSession *ClientSession) EnableShellMode() {
	clientSession.ShellMode = true
}

func (clientSession *ClientSession) DisableShellMode() {
	clientSession.ShellMode = false
}

// Private Scope

func (clientSession *ClientSession) instantiateSession() {
	// Get OS
	cmd := "echo $?"

	output := clientSession.sendJobCommand(cmd, ";")
	clientSession.OS = "Windows"
	switch {
	case output[0] == "True" || output[0] == "False":
		clientSession.Terminal = "Powershell"
	case output[0] == " ; echo $? ; echo ": // Looks messy ? optimizing the output would cost too much useless operations
		clientSession.Terminal = "Console"
		// Get proper shell
		clientSession.spawnPowershell()
		clientSession.resizePowershellTty()
	default:
		clientSession.OS = "Linux"
		terminal := clientSession.detectBash()
		if terminal == "/bin/sh" {
			clientSession.Terminal = "sh"
		} else {
			clientSession.Terminal = "bash"
		}
		// Get proper shell
		clientSession.spawnPty(terminal)
	}
	// Get User
	clientSession.RemoteUser = clientSession.sendJobCommand("whoami", ";")[0]
}

func (clientSession *ClientSession) sendJobCommand(cmd, separator string) []string {
	clientSession.DisableShellMode()
	defer clientSession.EnableShellMode()
	head := GenerateRandomString(10)
	tail := GenerateRandomString(10)

	FullCmd := fmt.Sprintf("echo %s "+separator+" %s "+separator+" echo %s "+separator, head, cmd, tail)
	clientSession.SendCommand(FullCmd)
	return clientSession.receiveJobCommand(head, tail)
}

func (clientSession *ClientSession) receiveJobCommand(head, tail string) []string {
	const AnsiCharactersSet = "[\u001B\u009B][[\\]()#;?]*(?:(?:(?:[a-zA-Z\\d]*(?:;[a-zA-Z\\d]*)*)?\u0007)|(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PRZcf-ntqry=><~]))"
	IsRawResponse := false
	var RawResponse []string
	for {
		response := <-clientSession.ClientOutput
		Split := strings.Split(response, "\n")
		for i := range Split {
			clear := strings.TrimSpace(regexp.MustCompile(AnsiCharactersSet).ReplaceAllString(Split[i], ""))

			if strings.HasPrefix(clear, head) {
				// Sometime a command doesn't end with EOL. If so, remove the tail payload from Clear and the rest of the shell.
				IsRawResponse = true
				clear = strings.Split(clear, head)[1]
				if clear == "" {
					continue
				}
			}

			if clear == tail {
				return RawResponse
			}
			if IsRawResponse {
				if clear == "\n" || clear == "" {
					continue
				}
				if strings.Contains(clear, tail) {
					// Sometime a command doesn't end with EOL. If so, remove the tail payload from Clear and the rest of the shell.
					clear = strings.Split(clear, tail)[0]
					RawResponse = append(RawResponse, clear)
					return RawResponse
				}
				RawResponse = append(RawResponse, clear)
			}
		}
	}
}

func GenerateRandomString(length int) string {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}
