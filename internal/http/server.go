package http

import (
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"github.com/fatih/color"
	"github.com/gorilla/mux"
)

// This package provides all the utilities needed to create an HTTP service for Torment.
// Features:
// - Hosting a list of files for download
// - Handling POST requests
// - Handling GET requests with parameters in the URL
// - Every requests are logged in project_directory/http(s).log

// Public Scope

// HttpConfiguration represents the configuration for the HTTP service.
type HttpConfiguration struct {
	ListenAddr   string              // IP address to listen on
	ListenPort   int                 // Port to listen on
	WebRoot      string              // Directory to host
	LogDirectory string              // Path where logs will be written
	httpsConf    *HttpsConfiguration // HTTPS configuration
	server       *http.Server        // HTTP server instance
	protocol     string              // Protocol used (http or https)
	extracter    chan *extractData   // Channel for real-time information extraction
}

// HttpsConfiguration represents the HTTPS configuration for the HTTP service.
type HttpsConfiguration struct {
	TlsConf  *tls.Config // TLS configuration
	CertFile string      // Path to the certificate file
	KeyFile  string      // Path to the private key file
}

// Server Constructor.
func NewHTTPConfiguration(logDirectory, listenAddr, webRoot string) *HttpConfiguration {
	parsedAddr := strings.Split(listenAddr, ":")
	port, _ := strconv.Atoi(parsedAddr[1])

	return &HttpConfiguration{
		ListenAddr:   parsedAddr[0],
		ListenPort:   port,
		WebRoot:      webRoot,
		LogDirectory: logDirectory,
		httpsConf:    &HttpsConfiguration{},
		server:       nil,
		extracter:    make(chan *extractData),
	}
}

// CertificatePath sets the path to the certificate file in the HTTPS configuration.
func (httpConf *HttpConfiguration) CertificatePath(path string) *HttpConfiguration {
	httpConf.httpsConf.CertFile = path
	return httpConf
}

// KeyPath sets the path to the private key file in the HTTPS configuration.
func (httpConf *HttpConfiguration) KeyPath(path string) *HttpConfiguration {
	httpConf.httpsConf.KeyFile = path
	return httpConf
}

// TLSConf sets the TLS configuration in the HTTPS configuration.
func (httpConf *HttpConfiguration) TLSConf(TLSConf *tls.Config) *HttpConfiguration {
	httpConf.httpsConf.TlsConf = TLSConf
	return httpConf
}

// Start initiates the HTTP service.
// Checks if the webRoot directory exists and if HTTPS variables are correct before starting.
func (serverInstance *HttpConfiguration) Start() {
	if _, err := os.Stat(serverInstance.WebRoot); os.IsNotExist(err) {
		fmt.Println("Directory", serverInstance.WebRoot, "does not exist.")
		return
	}
	if *serverInstance.httpsConf != (HttpsConfiguration{}) {
		if serverInstance.httpsConf.CertFile == "" && serverInstance.httpsConf.KeyFile == "" && serverInstance.httpsConf.TlsConf == nil {
			log.Println("Please supply either files or a TLS configuration.")
			return
		}
		if serverInstance.httpsConf.CertFile != "" && serverInstance.httpsConf.KeyFile != "" && serverInstance.httpsConf.TlsConf != nil {
			log.Println("Too many arguments supplied.")
			return
		}
	}
	serverInstance.start()
}

// Stop halts the HTTP service.
func (serverInstance *HttpConfiguration) Stop() {
	if serverInstance.server != nil {
		fmt.Println("Closing HTTP server.")
		if err := serverInstance.server.Shutdown(context.Background()); err != nil {
			log.Fatal(err)
		}
		serverInstance = nil
		return
	}
	fmt.Println("The HTTP(s) service is not running")
}

// Restart stops and then starts the HTTP service.
func (serverInstance *HttpConfiguration) Restart() {
	serverInstance.Stop()
	serverInstance.start()
}

// Watch monitors real-time information extracted from /extract.
// Press ctrl+C to exit the function.
func (serverInstance *HttpConfiguration) Watch() {
	sigChan := make(chan os.Signal, 1)
	defer signal.Stop(sigChan)
	signal.Notify(sigChan, syscall.SIGINT)

	d := color.New(color.FgCyan, color.Bold)

	d.Println("Starting Watcher mode :")
	for {
		select {
		case sig := <-sigChan:
			if sig == syscall.SIGINT {
				d.Println("\nSIGINT received, exiting Watcher mode.")
				return
			}
		case extractRequest := <-serverInstance.extracter:
			if extractRequest.reqValues != nil {
				d.Printf("%s received from %s:\n", extractRequest.httpVerb, extractRequest.ip)
				for k, v := range extractRequest.reqValues {
					fmt.Println(k, " => ", v)
				}
			}
		}
	}
}

// Private Scope

// Represents data on requests to /extract.
type extractData struct {
	ip        string
	httpVerb  string
	reqValues map[string][]string
}

// handleWebRoutes instantiate all the routes needed for the server
func (serverInstance *HttpConfiguration) handleWebRoutes() *mux.Router {
	muxRouter := mux.NewRouter()
	muxRouter.HandleFunc("/extract", serverInstance.handleExtract)
	muxRouter.HandleFunc("/301/redirect", redirect)
	muxRouter.HandleFunc("/301/metadata", metadata)
	muxRouter.HandleFunc("/301/metadata6", metadata6)
	muxRouter.HandleFunc("/301/localhost", localhost)
	muxRouter.HandleFunc("/301/zeroes", zeroes)
	muxRouter.HandleFunc("/301/passwd", passwd)
	muxRouter.HandleFunc("/301/services", services)
	muxRouter.HandleFunc("/301/environ", environ)
	muxRouter.HandleFunc("/301/", docs)
	muxRouter.HandleFunc("/301/{id:[0-9]+}", nid)
	muxRouter.HandleFunc("/301/redirect", redirect).Methods("POST")
	muxRouter.HandleFunc("/301/metadata", metadata).Methods("POST")
	muxRouter.HandleFunc("/301/metadata6", metadata6).Methods("POST")
	muxRouter.HandleFunc("/301/localhost", localhost).Methods("POST")
	muxRouter.HandleFunc("/301/zeroes", zeroes).Methods("POST")
	muxRouter.HandleFunc("/301/passwd", passwd).Methods("POST")
	muxRouter.HandleFunc("/301/services", services).Methods("POST")
	muxRouter.HandleFunc("/301/environ", environ).Methods("POST")
	muxRouter.HandleFunc("/301/", docs).Methods("POST")
	muxRouter.HandleFunc("/301/{id:[0-9]+}", nid).Methods("POST")
	fs := http.FileServer(http.Dir(serverInstance.WebRoot))
	muxRouter.PathPrefix("/").Handler(fs)
	return muxRouter
}

// Start initiates the service.
// Addr: the address on which the service will listen.
// WebRoot: The directory to be hosted.
func (serverInstance *HttpConfiguration) start() {
	if *serverInstance.httpsConf == (HttpsConfiguration{}) {
		serverInstance.protocol = "http"
	} else {
		serverInstance.protocol = "https"
	}
	muxRouter := serverInstance.handleWebRoutes()
	logHandler := serverInstance.logRequestHandler(muxRouter)

	listenAddr := fmt.Sprintf("%s:%d", serverInstance.ListenAddr, serverInstance.ListenPort)
	serverInstance.server = &http.Server{Addr: listenAddr, Handler: logHandler, ErrorLog: log.New(io.Discard, "", 0)}
	serverInstance.serve()
}

// Serve starts the HTTP server.
func (serverInstance *HttpConfiguration) serve() {
	listenAddr := fmt.Sprintf("%s:%d", serverInstance.ListenAddr, serverInstance.ListenPort)
	log.Printf("Listening on %s, WebRoot -> %s.\n", listenAddr, serverInstance.WebRoot)
	var err error
	if serverInstance.protocol == "https" {
		if serverInstance.httpsConf.TlsConf != nil {
			serverInstance.server.TLSConfig = serverInstance.httpsConf.TlsConf
			err = serverInstance.server.ListenAndServeTLS("", "")
		} else if serverInstance.httpsConf.KeyFile != "" {
			err = serverInstance.server.ListenAndServeTLS(serverInstance.httpsConf.CertFile, serverInstance.httpsConf.KeyFile)
		}
	} else {
		err = serverInstance.server.ListenAndServe()
	}

	if err != nil && err == http.ErrServerClosed {
		return
	}

	if err != nil {
		fmt.Println(err)
		return
	}
}

// Handles POST and GET parameters on /extract when dealing with csrf/ssrf.
func (serverInstance *HttpConfiguration) handleExtract(w http.ResponseWriter, r *http.Request) {
	var reqValues map[string][]string = nil
	var reqValuesString string
	// Since URL parameters and POST bodies are in the same format, use the URL parameter parser to parse POST data.
	// This way, they are both in the same type, making it easier to use them.
	if r.Method == "GET" {
		reqValuesString = r.URL.RawQuery
	} else if r.Method == "POST" && r.Header.Get("Content-type") == "application/x-www-form-urlencoded" {
		bytedata, _ := io.ReadAll(r.Body)
		reqValuesString = string(bytedata)
	} else {
		return
	}

	reqValues, _ = url.ParseQuery(reqValuesString)

	select {
	// Use select and case statement to prevent filling server.extracter if already full.
	case serverInstance.extracter <- &extractData{ip: r.RemoteAddr, reqValues: reqValues, httpVerb: r.Method}:
		return
	default:
		return
	}
}
