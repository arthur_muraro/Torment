# syntax=docker/dockerfile:1
FROM debian:latest

# Install dependency for gopacket and go install
RUN apt-get update -y
RUN apt-get install -y --no-install-recommends ca-certificates \
libpcap-dev \
wget \
rpm \
gcc make \
build-essential libssl-dev zlib1g-dev \
libbz2-dev libreadline-dev libsqlite3-dev curl \
libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

# Set destination for COPY
WORKDIR /torment
COPY build/ ./build/

# Install Python3.11
WORKDIR /torment/build
RUN tar -xzf ./Python-3.11.0.tgz
WORKDIR /torment/build/Python-3.11.0/
RUN ./configure --enable-optimizations
RUN make altinstall
RUN update-alternatives --install /usr/bin/python python /usr/local/bin/python3.11 1

WORKDIR /torment
# Install golang
RUN rm -rf /usr/local/go && tar -C /usr/local -xzf ./build/go1.21.0.linux-amd64.tar.gz
RUN ln -s /usr/local/bin/go /usr/bin/go
ENV PATH=$PATH:/usr/local/go/bin

# Install nmap
RUN rpm -vhU --nodeps ./build/nmap-7.94-1.x86_64.rpm

# Download Go modules
COPY go.mod go.sum ./
COPY vendor/ ./vendor/
RUN go mod download
COPY cmd/ ./cmd/

# Build
WORKDIR /torment/cmd/Torment/
RUN CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o /torment .

# Run
ENTRYPOINT  ["/torment/Torment"]