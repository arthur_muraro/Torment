module gitlab.com/arthur_muraro/Torment

go 1.20

require (
	github.com/Ullaakut/nmap/v3 v3.0.2
	github.com/desertbit/grumble v1.1.3
	github.com/fatih/color v1.15.0
	github.com/jedib0t/go-pretty/v6 v6.4.6
	golang.org/x/term v0.11.0
)

require (
	github.com/desertbit/closer/v3 v3.1.2 // indirect
	github.com/desertbit/columnize v2.1.0+incompatible // indirect
	github.com/desertbit/go-shlex v0.1.1 // indirect
	github.com/desertbit/readline v1.5.1 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
)
