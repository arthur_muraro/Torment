package app

import (
	"log"
	"os"

	"github.com/desertbit/grumble"
	"gitlab.com/arthur_muraro/Torment/internal/http"
)

func SetCommandHttp(GrumbleCLI *grumble.App) {
	GrumbleCLI.AddCommand(&grumble.Command{
		Name:    "HTTP",
		Help:    "Start and Stop services",
		Aliases: []string{"Http", "http"},

		Flags: func(f *grumble.Flags) {
			f.String("s", "start", "null", "Start the server with the provided listener string : ADDR:PORT.")
			f.String("p", "path", "null", "Used to set the path of the webroot")
			f.Bool("t", "stop", false, "Stop the server.")
			f.Bool("w", "watch", false, "Start the Watcher mode to print in real time post requests bodies.")
		},

		Run: func(c *grumble.Context) error {

			// For http --stop
			if c.Flags.Bool("stop") {
				HttpServer.Stop()
			}

			// For http --start ADDR:IP [ --path PATH ]
			if c.Flags.String("start") != "null" {
				var path string
				if c.Flags.String("path") != "null" {
					path = c.Flags.String("path")
				} else {
					var err error
					// if no path is provided, the server is started on the current directory
					path, err = os.Getwd()
					if err != nil {
						log.Fatal(err)
					}
				}
				HttpServer = http.NewHTTPConfiguration(ProjectPath, c.Flags.String("start"), path)
				go HttpServer.Start()
				return nil
			}

			// For http --path PATH
			if c.Flags.String("path") != "null" && c.Flags.String("start") == "null" {
				HttpServer.WebRoot = c.Flags.String("path")
				go HttpServer.Restart()
			}

			// For http -w
			if c.Flags.Bool("watch") {
				HttpServer.Watch()
			}

			return nil
		},
	})
}
