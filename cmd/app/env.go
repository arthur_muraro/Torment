package app

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/arthur_muraro/Torment/internal/http"
	"gitlab.com/arthur_muraro/Torment/internal/tcp"
)

var TormentPath string = GetHomeDir() + "/.Torment/"
var ProjectPath string = TormentPath + GetCurrentDir()
var TargetsPath string = ProjectPath + "/targets.json"
var HttpServer *http.HttpConfiguration = &http.HttpConfiguration{}
var HttpsServer *http.HttpConfiguration = &http.HttpConfiguration{}
var TcpServer *tcp.TcpConfiguration = &tcp.TcpConfiguration{}

func LaunchEnv() {
	CreateTormentEnv()
	CreateProjectEnv()
	CreateTargetsFile()
	CreateNmapDirectory()
}

func GetHomeDir() string {
	HomeDir, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	return HomeDir
}

func CreateTormentEnv() {
	if err := MakeDirectory(TormentPath); err != nil {
		fmt.Println(err.Error())
	}
}

func GetCurrentDir() string {
	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	CurrentDir := filepath.Base(cwd)
	return CurrentDir
}

func CreateProjectEnv() {
	if err := MakeDirectory(ProjectPath); err != nil {
		fmt.Println(err.Error())
	}
}

func CreateTargetsFile() {
	var TargetsPath string = ProjectPath + "/targets.json"
	if err := makeFile(TargetsPath); err != nil {
		fmt.Println(err.Error())
	}
}

func CreateNmapDirectory() {
	var NmapDirectoryPath string = ProjectPath + "/nmap"
	if err := MakeDirectory(NmapDirectoryPath); err != nil {
		fmt.Println(err.Error())
	}
}

func MakeDirectory(path string) error {
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		return fmt.Errorf("directory %s already exists", path)
	}

	// Create project directory
	err := os.Mkdir(path, 0750)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}

func makeFile(path string) error {
	// Check if Targets file exists
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		return fmt.Errorf("file %s already exists", path)
	}

	// Create Targets file
	filePtr, err := os.Create(path)
	if err != nil {
		log.Fatal(err)
	}

	// Set POSIX rights
	err = os.Chmod(path, 0640)
	if err != nil {
		log.Fatal(err)
	}

	defer filePtr.Close()
	return nil
}
