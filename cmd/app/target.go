package app

import (
	"fmt"
	"strings"

	"github.com/desertbit/grumble"
	"github.com/jedib0t/go-pretty/v6/table"

	"gitlab.com/arthur_muraro/Torment/internal/target"
	"gitlab.com/arthur_muraro/scope-manager/pkg/json"
)

func SetCommandTargets(GrumbleCLI *grumble.App) {
	GrumbleCLI.AddCommand(&grumble.Command{
		Name:    "target",
		Help:    "add, remove and list targets and targets",
		Aliases: []string{"t"},

		Flags: func(f *grumble.Flags) {
			f.String("a", "add", "null", "add a target (IP, or hostname / FQDN as long as the system can resolve it.), with it's corresponding alias")
			f.String("d", "delete", "null", "delete a target (IP, or hostname / FQDN as long as the system can resolve it.)")
			f.Bool("l", "list", false, "list targets")
		},

		Run: func(c *grumble.Context) error {
			// For: target --add
			if c.Flags.String("add") != "null" {
				var SplitedInput []string = strings.Split(c.Flags.String("add"), ",")
				for i := range SplitedInput {
					// Handle empty values
					if len(SplitedInput[i]) == 0 {
						continue
					}
					// Parse l'argument TargetAlias dans IP et Alias en utilisant le caractère "=" comme séparateur
					parts := strings.Split(SplitedInput[i], "=")
					if len(parts) != 2 {
						fmt.Println("L'argument Targets doit être de la forme 'IP=Alias'.")
						continue
					}
					target.AddTarget(TargetsPath, parts[0], parts[1])
				}
			}

			// For: target --delete
			if c.Flags.String("delete") != "null" {
				input := strings.Split(c.Flags.String("delete"), ",")
				for i := range input {
					target.RemoveTarget(TargetsPath, input[i])
				}
			}

			// For: target --list
			if c.Flags.Bool("list") {
				targets := *json.ReadGenericArrayFromJson[target.Target](TargetsPath)
				// diplay the list of targets via table package
				t := table.NewWriter()
				t.SetStyle(table.StyleLight)
				t.SetTitle("Saved Targets")
				t.AppendHeader(table.Row{"Hostname - IP", "Alias"})
				for _, item := range targets {
					t.AppendRow(table.Row{item.Host, item.Alias})
				}
				c.App.Println(t.Render())
			}

			return nil
		},
	})
}
