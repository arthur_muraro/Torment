package app

import (
	"github.com/desertbit/grumble"
)

func LaunchGrumble() {
	var GrumbleCLI *grumble.App = SetGrumble()

	SetCommandTargets(GrumbleCLI)
	SetCommandNmap(GrumbleCLI)
	SetCommandTcp(GrumbleCLI)
	SetCommandSessions(GrumbleCLI)
	SetCommandHttp(GrumbleCLI)
	SetCommandHttps(GrumbleCLI)
	grumble.Main(GrumbleCLI)
}

func SetGrumble() *grumble.App {
	// set flags and prompt
	var GrumbleCLI = grumble.New(&grumble.Config{
		Name:        "Torment",
		Description: "Torment - Yet another C2 aimed to bring eternal torments to your enemies... (evil laugh)",

		Flags: func(f *grumble.Flags) {
			f.String("d", "directory", ProjectPath, "set an alternative directory path")
			f.Bool("v", "verbose", false, "enable verbose mode")
		},
	})

	// set banner
	GrumbleCLI.SetPrintASCIILogo(func(a *grumble.App) {
		a.Println("  :.          :        ....                                                     :                                    ")
		a.Println("  P@@@@&&&@@@@B     ^P&@@@@@#7      .B@&&&&G:      .#@&@@@&&@@@@&&!      P@&@@@@@     :&@#.  .#@#.        .#@&!      ")
		a.Println("  ?J777@@@?!7?J    5@@Y:&@~~&@&.     G@@^ B@@.      #@@?~Y@@#~~&@@.      B@@?^^^?      &@@#7  #@#     ..   #@@:   .  ")
		a.Println("       &@@        :@@&PG@@BPB@@5     G@@#B@@J       #@@. ^@@P  G@@:    .@@@@&####      &@@@@@J&@#     ?@&&&@@@@&&@&  ")
		a.Println("       &@@         &@&~.&@~:G@@~     G@@5#@@P:      #@@. ^@@P  B@@:    .Y&@@Y!!!Y      &@G.P@@@@#     !5???&@@5???5  ")
		a.Println("       @@@.        .P@@#@@#&@#~      B@@^ :G@@#!    &@@. ~@@G  #@@:      G@@&BBBB      @@B  .J@@&          #@@:      ")
		a.Println("      :?J?^          .^?YYJ!.       .7JJ~   .?JJ~  .?JJ^ ~JJ7..?JJ^      B@5JJJJG     :?J7.  .?J?.        .?JJ^      ")
		a.Println("                                                                         :                                           ")
		// a.Println("                   ..~?Y!.         .!Y?^.                                                                            ")
		// a.Println("                 :?PB#G^             ^G#BP7:                                                                         ")
		// a.Println("              .7G&&&&?.               .J&&&&G7.                                                                      ")
		// a.Println("          . :5&@@@@&?                   J&@@@@#Y:                                                                    ")
		// a.Println("          .Y&&@@@@&P.                   .G@@@@@&&J.           - Yet another C2 aimed to bring torments to            ")
		// a.Println("        .J#@@@@@@@&5                    .P&@@@@@@&#?.         		your enemies                                    ")
		// a.Println("       ^B&@@@@@@@@@5 :BB?    ^^^    ?#G. P@@@@@@@@@&G^                                                               ")
		// a.Println("      !B&&&@@@@@@@@#^ 5&&BJ:     :Y#&&Y ^#@@@@@@@@&&&B~                                                              ")
		// a.Println("     :Y?~~^P@@@@@@@@#^ ?#&&&G7^?B&&&B7 ~#@@@@@@@@5:~~?Y:                                                             ")
		// a.Println("           .G&@@@@@@@&J `J#@@@@@@@#?  Y&@@@@@@@&P.                                                                   ")
		// a.Println("            ^P#@@@@@@@@#7 :B@@@@@G: ?&@@@@@@@@#5:                                                                    ")
		// a.Println("             :P@@@@#&@@@@&P#@@@@@#P&@@@@##@@@@5.                                                                     ")
		// a.Println("             5&@@@@7 :7B@@@@@@@@@@@@@G7. ?@@@@&Y                                                                     ")
		// a.Println("             ?&@@@@&!   :G@@@@@@@@@P:   !&@@@@&7      \\                                                             ")
		// a.Println("             :B&@@@@@#PPG&@@@@@@@@@&GPP#@@@@@&G:       \\                                                            ")
		// a.Println("              :7B&@@@@@@@@@&B&@&B&@@@@@@@@@&G!.         \\                                                           ")
		// a.Println("                 ~#&@@@@@@@#. ~ :#@@@@@@@&B^             \\                                                          ")
		// a.Println("                 .P&G7J&@@@@#: ^&@@@@&?7B&5               \\                                                         ")
		// a.Println("                 .G#7  5&&@@@&G@@@@&&J  ?#P                                                                          ")
		// a.Println("                 !#B^  :.^G@@@@@@&P^..  ^BB~                EVIL LAUGH !!!                                           ")
		// a.Println("                .G&B~^~   .P&@@@&P.   !^!#&P.                                                                        ")
		// a.Println("                Y&@&&&J .. :~B&B~: .. Y&&&@&J                                                                        ")
		// a.Println("               5&&@@&Y. :... 7#~ ...: .5&@@&&J                                                                       ")
		// a.Println("             !B&&&&P~        :5         ~G&&&&G~                                                                     ")
		// a.Println("            ~#&&#Y^                       ^5#&&B~                                                                    ")
		// a.Println("            !GP7                            :?PG~                                                                    ")
	})

	return GrumbleCLI
}
