package app

import (
	"fmt"
	"os"

	"github.com/desertbit/grumble"

	"gitlab.com/arthur_muraro/Torment/internal/http"
)

func SetCommandHttps(GrumbleCLI *grumble.App) {
	GrumbleCLI.AddCommand(&grumble.Command{
		Name:    "HTTPS",
		Help:    "Start and Stop services",
		Aliases: []string{"Https", "https"},

		Flags: func(f *grumble.Flags) {
			f.String("s", "start", "null", "Start the server with the provided listener string : ADDR:PORT.")
			f.String("p", "path", "null", "Used to set the path of the webroot")
			f.Bool("t", "stop", false, "Stop the server.")
			f.Bool("w", "watch", false, "Start the Watcher mode to print in real time post requests bodies.")
			f.String("c", "CertFile", "", "Path to certificate file. Has to be gibenwith it's")
			f.String("k", "KeyFile", "", "Path to key file.")
		},

		Run: func(c *grumble.Context) error {
			// For https --stop
			if c.Flags.Bool("stop") {
				HttpsServer.Stop()
			}

			// for https --start ADDR:PORT [ --Certfile /path/to/Cert --KeyFile /path/to/Key ] [ --path PATH ]
			if c.Flags.String("start") != "null" {
				var path string
				if c.Flags.String("path") != "null" {
					path = c.Flags.String("path")
				} else {
					// if no path is provided, the server is started on the current directory
					path, _ = os.Getwd()
				}

				HttpsServer = http.NewHTTPConfiguration(ProjectPath, c.Flags.String("start"), path)

				if c.Flags.String("CertFile") != "" && c.Flags.String("KeyFile") != "" {
					HttpsServer.CertificatePath(c.Flags.String("CertFile"))
					HttpsServer.KeyPath(c.Flags.String("KeyFile"))
				} else {
					serverCert, err := http.CreateCert()
					if err != nil {
						fmt.Println(err)
						return nil
					}
					HttpsServer = HttpsServer.TLSConf(serverCert)
				}
				go HttpsServer.Start()

				return nil
			}

			// For https --path PATH
			if c.Flags.String("path") != "null" && c.Flags.String("start") == "null" {
				HttpsServer.WebRoot = c.Flags.String("path")
				go HttpsServer.Restart()
			}

			// For https -w
			if c.Flags.Bool("watch") {
				HttpsServer.Watch()
			}

			return nil
		},
	})
}
