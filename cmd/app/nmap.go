package app

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"sync"

	"github.com/Ullaakut/nmap/v3"
	"github.com/desertbit/grumble"
	"github.com/jedib0t/go-pretty/v6/table"

	"gitlab.com/arthur_muraro/Torment/internal/target"
)

func SetCommandNmap(GrumbleCLI *grumble.App) {
	GrumbleCLI.AddCommand(&grumble.Command{
		Name:    "nmap",
		Help:    "nmap",
		Aliases: []string{"n", "Nmap"},

		Args: func(a *grumble.Args) {
			a.String("hosts", "hosts to Interact with. Can be an IP, FQDN. Or a target or an alias to a target defined with the target command. The target must begin with an '@'", grumble.Default(""))
		},

		Flags: func(f *grumble.Flags) {
			f.Bool("s", "status", false, "List all the running scans and their progress.")
			f.Bool("r", "read", false, "read the entire nmap result associated with target provided")
			f.Bool("p", "ports", false, "Only read the ports of the nmap result associated with target provided")
			f.Bool("l", "list", false, "list all the nmaps done for the project")
		},

		Run: func(c *grumble.Context) error {

			if c.Args.String("hosts") != "" {
				Hosts := parseHosts(c.Args.String("hosts"))
				if len(Hosts) == 0 {
					fmt.Println("No valid target available, aborting scans.")
					return nil
				}

				// For : nmap target
				if !c.Flags.Bool("read") && !c.Flags.Bool("ports") {

					for i := range Hosts {
						ScanList = append(ScanList, &ScanThread{progress: make(chan float32), hostname: Hosts[i]})
						fmt.Printf("Nmap scan started for \x1b[32m%s\x1b[0m\n", Hosts[i])
						go Scan(Hosts[i], ScanList[len(ScanList)-1])
					}
				}

				// For : nmap --read target
				if c.Flags.Bool("read") {
					for i := range Hosts {
						ScanResult := ReadScanOutput(Hosts[i])

						// Afficher toutes les valeurs de toutes les sous-structures de "Port" de l'hôte
						for _, port := range *ScanResult {
							fmt.Printf("\x1b[32m%d/%-7s\x1b[0m\t\x1b[33m%s\x1b[0m\t\x1b[36m%s\x1b[0m\t\x1b[35m%s\x1b[0m\n",
								port.ID,
								port.Protocol,
								port.State.State,
								port.Service.Name,
								port.Service.Version)

							// Afficher les valeurs de chaque script associé au port (s'il y en a)
							for _, script := range port.Scripts {
								fmt.Printf("%s : %s\n", script.ID, script.Output)
							}
						}
					}
				}

				// For : nmap --ports target
				if c.Flags.Bool("ports") {
					for i := range Hosts {
						ScanResult := ReadScanOutput(Hosts[i])

						t := table.NewWriter()
						t.SetStyle(table.StyleLight)
						t.SetTitle("Result scan for %s", Hosts[i])
						t.AppendHeader(table.Row{"PORT", "STATE", "SERVICE", "REASON", "VERSION"})

						// Afficher toutes les valeurs de toutes les sous-structures de "Port" de l'hôte
						for _, port := range *ScanResult {
							t.AppendRow(table.Row{
								fmt.Sprintf("%d/%s", port.ID, port.Protocol),
								port.State.State,
								port.Service.Name,
								port.State.Reason,
								fmt.Sprintf("%s %s", port.Service.Product, port.Service.Version)})
						}
						c.App.Println(t.Render())
					}
				}
			}

			// For : nmap --status
			if c.Flags.Bool("status") {
				t := table.NewWriter()
				t.SetStyle(table.StyleLight)
				t.SetTitle("Running scan(s)")
				t.AppendHeader(table.Row{"Hostname", "Progress"})
				for _, item := range ScanList {
					t.AppendRow(table.Row{item.hostname, fmt.Sprintf("%v%%", item.GetProgress())})
				}
				c.App.Println(t.Render())
			}

			// For : nmap --list
			if c.Flags.Bool("list") {
				Hosts, Commands := ListScansResults()

				t := table.NewWriter()
				t.SetStyle(table.StyleLight)
				t.SetTitle("Available scans results")
				t.AppendHeader(table.Row{"Hostname", "Command line"})
				for i := range Hosts {
					t.AppendRow(table.Row{Hosts[i], Commands[i]})
				}
				c.App.Println(t.Render())
			}

			return nil
		},
	})
}

var ScanList []*ScanThread

type ScanThread struct {
	progress chan float32
	hostname string
	mux      sync.RWMutex
}

func (t *ScanThread) GetProgress() float32 {
	t.mux.RLock()
	defer t.mux.RUnlock()
	return <-t.progress
}

func Scan(Host string, Thread *ScanThread) {
	// Equivalent to `/usr/local/bin/nmap -vv -sS -sC -sV -p- -Pn targets`,
	// with a 5-minute timeout.
	// -sS is the default mode.
	s, err := nmap.NewScanner(
		context.Background(),
		nmap.WithSkipHostDiscovery(), // Equivalent -Pn
		nmap.WithTargets(Host),       // host
		nmap.WithPorts("1-65535"),    // Equivalent -p-
		nmap.WithDefaultScript(),     // Equivalent -sC
		nmap.WithServiceInfo(),       // Equivalent -sV
		nmap.WithVerbosity(2),        // Equivalent -vv
	)
	if err != nil {
		log.Fatalf("unable to create nmap scanner: %v", err)
	}

	// Executes asynchronously, allowing results to be streamed in real time.
	done := make(chan error)

	result, warnings, err := s.Progress(Thread.progress).Async(done).Run()
	if err != nil {
		log.Fatal(err)
	}

	// Blocks main until the scan has completed.
	if err := <-done; err != nil {
		if len(*warnings) > 0 {
			log.Printf("run finished with warnings: %s\n", *warnings) // Warnings are non-critical errors from nmap.
		}
		log.Fatal(err)
	}
	fmt.Printf("Scan for \x1b[32m%s\x1b[0m finished !\n", Host)

	// Remove the scan from the ScanList Once it's finished
	for i, item := range ScanList {
		if item.hostname == Host {
			ScanList[i] = ScanList[len(ScanList)-1]
			ScanList = ScanList[:len(ScanList)-1]
		}
	}

	result.ToFile(ProjectPath + "/nmap/" + Host + ".xml")
}

func ReadScanOutput(Host string) (ScanResult *[]nmap.Port) {
	var ResultPath string = ProjectPath + "/nmap/" + Host + ".xml"

	// Lire le fichier XML du résultat du scan
	XmlFile, err := os.Open(ResultPath)
	// if we os.Open returns an error then handle it
	if err != nil {
		log.Fatal(err)
	}

	XmlData, err := io.ReadAll(XmlFile)
	if err != nil {
		log.Fatal(err)
	}
	XmlFile.Close()

	var Content nmap.Run
	nmap.Parse(XmlData, &Content)

	return &Content.Hosts[0].Ports
}

func ListScansResults() (Hosts []string, Commands []string) {
	var ResultsPath string = ProjectPath + "/nmap/"

	// Récupération des hôtes scannés en listant le dossier de sortie des scans nmap
	entries, err := os.ReadDir(ResultsPath)
	if err != nil {
		log.Fatal(err)
	}

	for _, e := range entries {
		Filename := e.Name()                   // get Filename for later use
		Hostname := Filename[:len(Filename)-4] // get Filename without extension for return value

		Hosts = append(Hosts, Hostname)

		// Récupération et parsing de la 5ème ligne de chaque fichiers pour retourner plus d'informations à l'utilisateur sur les conditions des scans.
		file, err := os.Open(ResultsPath + "/" + Filename)
		if err != nil {
			log.Fatal("Erreur lors de l'ouverture du fichier :", err)
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		lineNumber := 1
		for scanner.Scan() {
			line := scanner.Text()
			if lineNumber == 5 {
				var XmlParse nmap.Run
				nmap.Parse([]byte(line), &XmlParse)
				Commands = append(Commands, XmlParse.Args)
				break
			}
			lineNumber++
		}
	}

	return Hosts, Commands
}

func parseHosts(hosts string) []string {
	// If host begins with an "@" check if it's a valid regitered target from the JSON database
	// return the concat of unregistered targets and valid registered hosts
	splitedHosts := strings.Split(hosts, ",")
	var validHosts []string

	for i := range splitedHosts {
		if splitedHosts[i][0] == '@' {
			Host := target.ResolveAlias(TargetsPath, splitedHosts[i][1:])
			if len(Host) == 0 {
				fmt.Printf("The target \x1b[31m%s\x1b[0m does not exist and will not be taken into account.\n", splitedHosts[i][1:])
				break
			}
			splitedHosts[i] = Host
		}
		validHosts = append(validHosts, splitedHosts[i])
	}
	return validHosts
}
