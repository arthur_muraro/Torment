package app

import (
	"github.com/desertbit/grumble"

	"gitlab.com/arthur_muraro/Torment/internal/tcp"
)

func SetCommandTcp(GrumbleCLI *grumble.App) {
	GrumbleCLI.AddCommand(&grumble.Command{
		Name:    "TCP",
		Help:    "Start and Stop services",
		Aliases: []string{"Tcp", "tcp"},

		Flags: func(f *grumble.Flags) {
			f.String("s", "start", "null", "Start the server with the provided listener string : ADDR:PORT.")
			f.Bool("t", "stop", false, "Stop the server.")

		},

		Run: func(c *grumble.Context) error {

			// For tcp --stop
			if c.Flags.Bool("stop") {
				TcpServer.Stop()
			}

			// For tcp --start ADDR:IP
			if c.Flags.String("start") != "null" {
				TcpServer = tcp.NewTcpConfiguration(c.Flags.String("start"))
				go TcpServer.Start()
			}

			return nil
		},
	})
}
