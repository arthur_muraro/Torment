package main

import (
	"gitlab.com/arthur_muraro/Torment/cmd/app"
)

func main() {
	app.LaunchEnv()

	app.LaunchGrumble()
}
