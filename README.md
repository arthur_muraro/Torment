# Torment

Yet another C2 aimed to bring torments to your enemies... *(evil laugh)*.

# Dependencies

- Go 1.17 ou plus
- Noyau Linux
- Nmap

# Credits

- The [301party](https://gitlab.com/wtfismyip/301party) project, that has been implemented to Torment.

# Environment

## Torment

Torment data is stored in JSON format. Some of this data pertains to its configuration, while others are related to "projects." The Torment folder is located in `homedir/.Torment`.

A project is created when Torment is launched. It is based on the current user's current directory to name the project. If the user is in `/path/to/pentest`, the project will be called `pentest` and will be located in `homedir/.Torment/pentest`.

## Project

The project contains:

- A file storing the targets named `hosts.json`
- A folder storing the results of nmap scans named nmap, containing files named `target.xml` according to the target's name.

## Environment Tree

```
homedir/.Torment/
└── pentest
    └── hosts.json
    └── nmap
        └── 192.168.1.10.xml
```

# Commands

## target

Allows you to define, delete, list, and rename pentest targets by interacting with the `homedir/pentest/targets.json` file.

### Nommage

- **Target**: contains both `Alias` and `Host` values. You can find this structure in the JSON persistent data as well as in the application's functionality.
- **Host**: corresponds to the Hostname or IP of the target.
- **Alias**: Alias is the nickname assigned to a target. By default, it is the same as the target's `Host`.

#### JSON Example

```json
{
  "targets": [
    {
      "host": "scanme.nmap.org",
      "alias": "scanme"
    },
    {
      "host": "127.0.0.1",
      "alias": "localhost"
    },
    {
      "host": "192.168.1.30",
      "alias": "FTP"
    },
    {
      "host": "192.168.1.20",
      "alias": "WebServer"
    }
  ]
}%
```

### Command example

```bash
Torment » target --add 192.168.1.10,DC.sec.local --delete old.sec.local --alias 192.168.1.10=WebServer,DC.sec.local=DC --list

Torment » t -a 192.168.1.10,DC.sec.local -d old.sec.local -y 192.168.1.10=WebServer,DC.sec.local=DC -l
```

Adds hosts `192.168.1.10` and `DC.sec.local`. Deletes `old.sec.local`. Lists registered targets in the database. Defines the alias `WebServer` for target `192.168.1.10` and `DC` for `DC.sec.local`.

![06e6a4a896f26a6962d4b145de544f21.png](./screens/06e6a4a896f26a6962d4b145de544f21-1.png)

Both commands perform the same action.

## nmap

Allows you to launch nmap scans. Scans are initiated based on user-selected `Targets`. The result of each scanned machine is stored in `homedir/pentest/targets.xml`.

### Command example

```bash
# Launch a scan on the localhost and scanme Targets.
nmap localhost,scanme
# Display information about ongoing scans.
nmap --status
# Display the types of scans and machines already scanned.
nmap --list
# Read the complete scan of the scanme Target.
nmap --read scanme
# Display only the ports and services scanned on the localhost Target.
nmap --ports localhost
```

![9c2c5af10e8723fdd56dc14c8043a9de.png](./screens/9c2c5af10e8723fdd56dc14c8043a9de-1.png)

![8e91ebe5713cc2b442e3c69b56e5a6f5.png](./screens/8e91ebe5713cc2b442e3c69b56e5a6f5-1.png)

![e4d97bbf0962f752581f4a00ff8c2b12.png](./screens/e4d97bbf0962f752581f4a00ff8c2b12-1.png)


## TCP

Interacts with the TCP service feature. This service is designed to capture connections from reverse shells.

### Command Example

```bash
# Start the server
tcp --start :4000
tcp -s 0.0.0.0:4000

# Stop the server
tcp --stop
tcp -t
```

### Features

As soon as a new client is received, the server automatically retrieves the user of the remote session and launches a full PTY.

### Technical Documentation

The implementation of the TCP server can be found in the `internal/tcp` directory of the source code. Each client is represented by a structure containing all the utilities to interact with them.

```go
// Represents a TCP Session handled by the server
type Session struct {
	Closed     chan struct{} // Used to detect when a session has ended
	OS         string        // Stores the OS name
	Id         int           // session's ID
	RemoteAddr string

	remotePort int
	remoteUser string      // Stores the user owner of the remote shell
	out        chan string // The channel used to pass output commands
	cmd        bool        // If the shell is cmd and not powershell
	ps1        string      // Stores the remote Pty's PS1
	shellMode  bool        // Used to detect when the shell is activated and so all the output has to be printed
	conn       net.Conn    // The TCP connection itself
	in         chan string // The channel used to pass input commands
}
```


The interaction with the server and clients occurs as follows:

- Start the server: `go tcp.Start(ListenAddr)`
- Stop the server: `tcp.Stop()`
- Retrieving a client based on an ID is done like this:

```go
client, err := tcp.GetClientById(SessionId)
if err != nil {
	// err
}
```

- To send a command to the client: `client.SendCommand(cmd)`. In this context, the received result will be printed on the screen.
- To send a command and retrieve its RawOutput (meaning without ANSI characters, command repetition, and shell prompt): `output := client.SendJobCommand(cmd)`

## Sessions

Sessions are directly related to the TCP server. This command is responsible for interacting with the clients' shells on the server.

### Command Example

```bash
# Display sessions
sessions --list
se -l
# Send a command to a client and retrieve the result in the terminal
sessions --command "echo 'I am so tormented rn :( !'" 1
se -c ls 2
# Start a shell on a session
sessions --shell 1
se -s 2
```

![screens/sessions_list.png](screens/sessions_list.png)

![screens/sessions_command.png](screens/sessions_command.png)

![screens/sessions_shell.png](screens/sessions_shell.png)

![screens/sessions_kill.png](screens/sessions_kill.png)

## HTTP

This command provides all the utilities needed to create an HTTP service for Torment.
Features:
- Hosting a list of files for download
- Handling POST requests
- Handling GET requests with parameters in the URL

You can provide to path to the directory you want to host (so victims can download tools).

### Command Example

```bash
# Start the server
http --start 0.0.0.0:8000 -p /path/to/webroot
http -s :8000 # If no path is provided, it will host the current working directory

# Update the webroot
http -p /path/to/new/webroot

# Stop the server
http --stop
http -t

# read extracted datas in real time
http -w
```

![screens/http.png](screens/http.png)

### Technical Documentation

The implementation of the HTTP server can be found in the `internal/http` directory of the source code. Here is how to interact with it.

- Start the server : `go http.Start(listenAddr, webRoot)`
- Stop the server : `http.Stop()`
- Update the webroot : `http.ChangeDirectory(webRoot)`
- Enter the watcher mod : `http.Watch()`. The watcher mode is an interact mode, press CTRL+C to leave it.

### Post exploitation scripts

