#!/bin/bash -
docker container stop torment
docker container rm torment
docker image rm torment -f
docker build --progress=plain -t torment .
bash ./run.sh